require("@nomicfoundation/hardhat-toolbox");
require("@nomiclabs/hardhat-etherscan");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: {
    // You can add more versions of Solidity
    compilers: [
      {
        version: "0.8.17",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  networks: {
    // Laotel Testnet 
    ltctestnet: { 
      url: `https://rpctestnet.laotel.com/`,
      accounts: [ 
        "", //Some Private Key of Wallet to Deploy Smart contract or You can use .ENV
      ],
    },
    // Binance Smartchain 
    bsctestnet: {
      url: "https://data-seed-prebsc-1-s3.binance.org:8545",
      accounts: [
        "", //Some Private Key of Wallet to Deploy Smart contract or You can use .ENV
      ],
    },
  },
  etherscan: {
    //must define apiKey for each network
    apiKey: { 
      ltctestnet: "none",
      bscTestnet: '7S7ZIBFNES713QQPP2J2TAUWFIAPW7K6ZF'
    },
    //Add Custom Chain for Laotel testnet
    customChains: [
      {
        network: "ltctestnet",
        chainId: 1999,
        urls: {
         apiURL: `https://ltctestnet.laotel.com/api`,
         browserURL: `https://ltctestnet.laotel.com/`,
        },
      },
    ],
  },
};
